# x-tools-smartg

Repository containing cross-compilation toolchain for the smart garden project

## Usage

Using this toolchain **REQUIRES** it to be located at

    /opt/x-tools-smartg

Any other location wont work, due to QT5 searching for hard coded paths.

It is not necessary to add this directory to your PATH variable, unless you
decide to use this toolchain for other projects, which is possible.

## Current Custom Libraries

* **Boost-1.72**:
    * Guaranteed includes:
        * unit_test_framework
    * Possible includes:
        * everything... didn't check every lib for functionality, but it should have it all
* **QT5.14**:
    * qtbase
